<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\DatosPersonales;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use app\models\Estudios;
use app\models\Experiencia;
use app\models\Otros;
use kartik\mpdf\Pdf;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //Consulta sin ejecutar
        //$consulta= DatosPersonales::find();
        //$modelo=$consulta->one();
        
        //Consulta ejecutada y lo que me devuelve es un modelo con los datos
        $modelo=DatosPersonales::find()->one();
        
        //Consulta ejecutada y lo que me devuelve es un array de modelos
        //$modelo=DatosPersonales::find()->all();
        //$modelo=$modelos[0];
        
        return $this->render('index',[
            "model"=>$modelo
        ]);
        
    }

    public function actionAcademica() {
        //Consulta de los estudios realizados
        $consulta=Estudios::find()->where([
            "complementarios"=>0
        ]);
        
        //DataProvider con el resultado demla consulta anterior
        //para mandarlo a un ListView
        $dataProvider=new ActiveDataProvider([
            "query"=>$consulta
        ]);
                
        return $this->render('academica',[
            "dataProvider"=>$dataProvider,
            "titulo"=>"Formación académica"
        ]);
    
    }
    
    public function actionComplementaria() {
        //Consulta de los estudios realizados
        $consulta=Estudios::find()->where([
            "complementarios"=>1
        ]);
        
        //DataProvider con el resultado demla consulta anterior
        //para mandarlo a un ListView
        $dataProvider=new ActiveDataProvider([
            "query"=>$consulta
            
        ]);
                
        return $this->render('academica',[
            "dataProvider"=>$dataProvider,
            "titulo"=>"Formación complementaria"
        ]);
    }
    
    public function actionExperiencia() {
        //Consulta de los estudios realizados
        $consulta=Experiencia::find();
        
        //DataProvider con el resultado demla consulta anterior
        //para mandarlo a un ListView
        $dataProvider=new ActiveDataProvider([
            "query"=>$consulta
            
        ]);
                
        return $this->render('experiencia',[
            "dataProvider"=>$dataProvider,
            "titulo"=>"Experiencia laboral"
        ]);
    }
    
    public function actionOtros() {
        $tipos=Otros::find()->select(["tipo"])->distinct();
        
        $dataProvider=new ActiveDataProvider([
            'query'=>$tipos
        ]);
        
        return $this->render('otros',[
            "dataProvider"=>$dataProvider,
            "titulo"=>"Otros datos"
        ]);
    }
    
    public function actionPdf(){
        // get your HTML raw content without any layouts or scripts
        $modelo=DatosPersonales::find()->one();
        
        $formacion= Estudios::find()->where([
            "complementarios"=>0
        ]);
        $dataProviderFormacion=new ActiveDataProvider([
           "query"=>$formacion 
        ]);
        
        $complementaria= Estudios::find()->where([
            "complementarios"=>1
        ]);
        $dataProviderComplementaria=new ActiveDataProvider([
            "query"=>$complementaria
        ]);
        
        $experiencia=Experiencia::find();
        $dataProviderExperiencia=new ActiveDataProvider([
            "query"=>$experiencia
        ]);
        
        $tipos=Otros::find()->select(["tipo"])->distinct();
        
        $dataProviderOtros=new ActiveDataProvider([
            'query'=>$tipos
        ]);
        
            
        
        $content = $this->renderPartial('pdf',[
            "model"=>$modelo,
            "formacion"=>$dataProviderFormacion,
            "complementaria"=>$dataProviderComplementaria,
            "experiencia"=>$dataProviderExperiencia,
            "otros"=>$dataProviderOtros
        ]);
    
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => ['@app/web/css/kv-mpdf-bootstrap.css','@app/web/css/custom.css'],
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Currículum'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }
}
