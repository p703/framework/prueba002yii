<?php
use yii\helpers\Html;
use yii\widgets\ListView;
?>

<div>
    
    <h1 style="float:left;width:15cm">
        <?= $model->getNombreCompleto() ?>
    </h1>

    <div style="float:right;width:3cm">
        <?= Html::img("@web/imgs/$model->foto",["class"=>"foto"]) ?>
    </div>
    
</div>

<div>
    <h3>
        Dirección
    </h3>
    <div>
        <?= $model->getDireccion() ?>
        <br>
        <?= $model->getPoblacion() ?>
    </div>
</div>

<div>
    <h3>
        Correo electrónico
    </h3>
    <div>
        <?= $model->email ?>
    </div>
</div>

<div>
    <h3>
        Teléfono
    </h3>
    <div>
        <?= $model->telefono ?>
    </div>
</div>

<div>
    <h3>
        Carnet
    </h3>
    <div>
        <?= $model->carnetConducir ?>
    </div>
</div>
<br>
<div>
    <h3>
        Formación
    </h3>
    <?php 
    echo ListView::widget([
       "dataProvider" =>$formacion,
       "itemView"=>"pdf/_formacion",
       "layout"=>"{items}",
       "options"=>["class"=>"caja"],
       "itemOptions"=>["class"=>"mb-3"]
    ]);
    ?> 
</div>

<div>
    <h3>
        Formación complementaria
    </h3>
    <?php 
    echo ListView::widget([
       "dataProvider" =>$complementaria,
       "itemView"=>"pdf/_formacion",
       "layout"=>"{items}",
       "options"=>["class"=>"caja"],
       "itemOptions"=>["class"=>"mb-3"]
    ]);
    ?> 
</div>

<div>
    <h3>
        Experiencia profesional
    </h3>
    <?php 
    echo ListView::widget([
       "dataProvider" =>$experiencia,
       "itemView"=>"pdf/_experiencia",
       "layout"=>"{items}",
       "options"=>["class"=>"caja"],
       "itemOptions"=>["class"=>"mb-3"]
    ]);
    ?> 
</div>

<div>
    <h3>
        Otros
    </h3>
    <?php 
    echo ListView::widget([
       "dataProvider" =>$otros,
       "itemView"=>"pdf/_otros",
       "layout"=>"{items}",
       "options"=>["class"=>"caja"],
       "itemOptions"=>["class"=>"mb-3"]
    ]);
    ?> 
</div>

