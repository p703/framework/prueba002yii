<?php
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

?>

<h2>
        <?= $titulo ?>
</h2>

<?= ListView::widget([
    "dataProvider"=>$dataProvider,
    "itemView"=>"_otros",
    "layout"=>"{items}",
    "options"=>["class"=>"caja"],
    "itemOptions"=>["class"=>"otros mb-3 pl-3"]
]);
?>

