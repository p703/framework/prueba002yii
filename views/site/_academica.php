    <div class="row">
        <div class="col-lg-2 mt-2">
            <?= $model->fechas ?>
        </div>
        <div class="col-lg-10 text-capitalize mt-2">
            <?= "<b>$model->titulo</b>" . ". (" . $model->centro . ")" ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
            
        </div>
        <ul class="col-lg-10 pl-5 mt-2 lista">
        <?php
            //foreach($model->getCompetencias()->all() as $competencia){
            foreach($model->competencias as $competencia){
                echo "<li>$competencia->descripcion</li>";
            }
        ?>
        </ul>
    </div>

