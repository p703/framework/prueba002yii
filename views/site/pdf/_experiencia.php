    <div>
        <div class="col-print-2 mt-2">
            <?= $model->fechas ?>
        </div>
        <div class="col-print-10 mt-2">
            <?= "<b>$model->empresa</b>" . " (" . $model->poblacion . "). " . $model->puesto . "." ?>
        </div>
    </div>

    <div>
        <div class="col-print-2">
            &nbsp;
        </div>
        <ul class="col-print-10 pl-5 mt-2">
        <?php
            foreach($model->funciones as $funcion){
                echo "<li>$funcion->descripcion</li>";
            }
        ?>
        </ul>
    </div>

