    <div>
        <div class="col-print-2 mt-2">
            <?= $model->fechas ?>
        </div>
        <div class="col-print-10 mt-2">
            <?= "<b>$model->titulo</b>" . ". (" . $model->centro . ")" ?>
        </div>
    </div>

    <div>
        <div class="col-print-2">
            &nbsp;
        </div>
        <ul class="col-print-10 pl-5 mt-2">
        <?php
            //foreach($model->getCompetencias()->all() as $competencia){
            foreach($model->competencias as $competencia){
                echo '<li><i class="fas fa-adjust"></i>' . $competencia->descripcion . "</li>";
            }
        ?>
        </ul>
    </div>

