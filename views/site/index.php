<?php
use yii\helpers\Html;

$this->title = 'Curriculum';
?>
<div class="site-index">

    
<?php
    /*echo yii\widgets\DetailView::widget([
        "model"=>$model,
        "attributes"=>[
            "telefono",
            "email",
            "carnetConducir",
            "calle",
            "numero",
            "poblacion",
            "cp"
        ],
        "template"=>function($atributo){
            return "<div>" . $atributo['label'] . "</div><div>" . $atributo['value'] . "</div>";
        }
    ]);*/
    

    /*foreach ($model as $label=>$campo){
        ?>
        <div><?= $label ?></div>
        <div><?= $campo ?></div>
        <?php
    }*/


    //USAR EL ARCHIVO DE WIDGETS/CARD EN CASO QUE QUERER QUE LA INFORMACION SALGA EN TARJETAS
    //Card::widget([
    //"titulo"=>"Nombre completo",
    //"contenido"=>$model->getDireccion() . "<br>" . $model->Poblacion() . "<br>" . $model->provincia;
    //]);
    //Esto delante de los getters: <div class="card col-lg-4 p-0 mt-2">
?>
    </div>



<?php?>
<h2><div><?= $model->getNombreCompleto() ?></div></h2>
<div class="row">
<div class="col-lg-2">
    <div class="mt-3"><?= Html::img("@web/imgs/$model->foto",["class"=>"img-fluid h-100"]); ?></div>
</div> 
<div class="col-lg-10">

<div class="mt-3"><b>Dirección:</b></div>
<div><?= $model->getDireccion() ?></div>
<div><?= $model->getPoblacion() ?></div>
<div class="mt-2"><b>Teléfono:</b></div>
<div><?= $model->telefono ?></div>
<div class="mt-2"><b>Mail:</b></div>
<div><?= $model->email ?></div>
<div class="mt-2"><b>Teléfono:</b></div>
<div><?= $model->telefono ?></div>
<div class="mt-2"><b>Carnet:</b></div>
<div><?= $model->carnetConducir ?></div>
</div>
</div>


