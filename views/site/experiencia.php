<?php
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

?>

<h2>
        <?= $titulo ?>
</h2>

<?= ListView::widget([
    "dataProvider"=>$dataProvider,
    "itemView"=>"_experiencia",
    "layout"=>"{items}",
    "options"=>["class"=>"caja"],
    "itemOptions"=>["class"=>"mb-3"]
]);
?>

