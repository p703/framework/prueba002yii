<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "experiencia".
 *
 * @property int $id
 * @property string|null $empresa
 * @property string|null $poblacion
 * @property string|null $puesto
 * @property string|null $fechas
 *
 * @property Funciones[] $funciones
 */
class Experiencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'experiencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa', 'fechas'], 'string', 'max' => 255],
            [['poblacion'], 'string', 'max' => 50],
            [['puesto'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa' => 'Empresa',
            'poblacion' => 'Poblacion',
            'puesto' => 'Puesto',
            'fechas' => 'Fechas',
        ];
    }

    /**
     * Gets query for [[Funciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFunciones()
    {
        return $this->hasMany(Funciones::className(), ['idExperiencia' => 'id']);
    }
}
