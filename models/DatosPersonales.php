<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "datos_personales".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $telefono
 * @property string|null $email
 * @property string|null $linkedin
 * @property string|null $carnetConducir
 * @property string|null $calle
 * @property int|null $numero
 * @property string|null $poblacion
 * @property string|null $cp
 * @property string|null $foto
 */
class DatosPersonales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'datos_personales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono', 'numero'], 'integer'],
            [['nombre', 'apellidos', 'calle', 'foto'], 'string', 'max' => 100],
            [['email', 'linkedin', 'poblacion'], 'string', 'max' => 30],
            [['carnetConducir'], 'string', 'max' => 50],
            [['cp'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'linkedin' => 'Linkedin',
            'carnetConducir' => 'Carnet Conducir',
            'calle' => 'Calle',
            'numero' => 'Numero',
            'poblacion' => 'Poblacion',
            'cp' => 'Cp',
            'foto' => 'Foto',
        ];
    }
    
    public function getNombreCompleto() {
        return "$this->nombre $this->apellidos";
    }
    
    public function getDireccion(){
        return "$this->calle, $this->numero";
    }
    
    public function getPoblacion(){
        return "$this->cp $this->poblacion";
    }
}
