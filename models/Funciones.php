<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "funciones".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property int|null $idExperiencia
 *
 * @property Experiencia $idExperiencia0
 */
class Funciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'funciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idExperiencia'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
            [['idExperiencia'], 'exist', 'skipOnError' => true, 'targetClass' => Experiencia::className(), 'targetAttribute' => ['idExperiencia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'idExperiencia' => 'Id Experiencia',
        ];
    }

    /**
     * Gets query for [[IdExperiencia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdExperiencia0()
    {
        return $this->hasOne(Experiencia::className(), ['id' => 'idExperiencia']);
    }
}
